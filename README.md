# Project StackOverShoes
StackOverShoes is a web application that will fulfill all of your footwear fashion needs.

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/achmad.afriza/group-assignment-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/achmad.afriza/group-assignment-2-ppw/commits/master)
[![coverage report](https://gitlab.com/achmad.afriza/group-assignment-2-ppw/badges/master/coverage.svg)](https://gitlab.com/achmad.afriza/group-assignment-2-ppw/commits/master)

## Group Members
**Abimanyu Yuda Dewa**
**Achmad Afriza**
**Adham Miftah**
**Al Taaj Kausar**
**Bagus Prabowo**

# Built With
- Django framework
- Visual Studio Code

# Link 
[https://stackovershoes2.herokuapp.com/](https://stackovershoes2.herokuapp.com/)