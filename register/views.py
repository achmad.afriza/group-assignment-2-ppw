from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import RegistrationForm, UpdateForm, ImageUpdate

# Create your views here.

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            first_name = form.cleaned_data.get('first_name')
            messages.success(request, f'You Have Created An Account, {first_name}, Now You Can Log In!')
            return redirect('/login')
        
    else:
        form = RegistrationForm()
    return render(request, 'register/register.html', {'form' : form})

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UpdateForm(request.POST, instance=request.user)
        p_form = ImageUpdate(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your Account Has Been Updated!')
            return redirect('/profile')

    else:
        u_form = UpdateForm(instance=request.user)
        p_form = ImageUpdate(instance=request.user.profile)

    context ={ 'u_form':u_form, 'p_form':p_form }

    return render(request, 'register/profile.html', context)