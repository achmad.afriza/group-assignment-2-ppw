from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.core import validators, serializers
from django.urls import reverse
from django.http import JsonResponse
from django.http import HttpResponse
import json

from cart.models import Order, OrderItem
from products.models import Product
from register.models import Profile
from .models import CouponCut, History
from .forms import addHistory

# Create your views here.
def index(request):
    context = {}
    if request.method == 'GET':
        try:
            items = Order.objects.get(ref_code=request.GET.get('ref_code')).items.all()
        except Order.DoesNotExist:
            return redirect('')
        totalprice = 0
        for OrderedItem in items:
            totalprice += OrderedItem.product.price
        
        context["items"] = items
        context['totalprice'] = totalprice
        context['ref_code'] = request.GET.get('ref_code')
    
    return render(request, 'checkout.html', context)

def getcoupon(request):
    if request.is_ajax and request.method == 'GET':
        coupon = CouponCut.objects.get(couponcode=request.GET.get('couponcode'))
        obj = serializers.serialize('json', [coupon, ])
        return JsonResponse({"response": obj}, status=200)

    return JsonResponse({"error": "Wrong Access"}, status=400)

def pay(request):
    if request.is_ajax and request.method == 'POST':
        order = Order.objects.get(ref_code=request.POST.get('ref_code'))
        try:
            coupon = CouponCut.objects.get(couponcode=request.POST.get('couponcode'))
            totalprice = request.POST.get('totalprice') * (1-coupon.discount)
        except:
            coupon = None
            totalprice = request.POST. get('totalprice')
        
        data = {
            'order': order,
            'coupon': coupon,
            'address': request.POST.get('address'),
            'totalprice': totalprice
        }
        form = addHistory(data)

        print(form.is_valid())
        print(data)
        if form.is_valid():
            form.save()
            return JsonResponse({"success": "Payment Successful"}, status=200)
        else:
            return JsonResponse({"error": "Invalid Form"}, status=400)

    return JsonResponse({"error": "Wrong Access"}, status=400)

def history(request):
    if request.method == 'GET':
        user_profile = get_object_or_404(Profile, user=request.user)
        history = History.objects.filter(user=user_profile)

        context = {
            'history': history,
        }

        return render(request, 'history.html', context)