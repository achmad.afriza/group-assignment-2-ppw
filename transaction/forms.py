from django import forms
from .models import History

class addHistory(forms.ModelForm):
    class Meta:
        model = History
        fields = {
            'order',
            'coupon',
            'address',
            'totalprice'
        }