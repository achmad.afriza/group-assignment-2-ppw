from django.db import models
from cart.models import Order
from register.models import Profile

# Create your models here.
class CouponCut(models.Model):
    name = models.CharField(max_length=15)
    couponcode = models.CharField(max_length=15)
    discount = models.FloatField()

class History(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    coupon = models.ForeignKey(CouponCut, on_delete=models.SET_NULL, null=True, blank=True)
    totalprice = models.IntegerField()
    address = models.TextField()
    order_date = models.DateTimeField(auto_now_add=True)