from django.urls import path, include
from django.conf.urls import url
from . import views

app_name = 'transaction'

urlpatterns = [
    path('', views.index, name='index'),
    path('API/getcoupons', views.getcoupon, name='getcoupon'),
    path('API/pay', views.pay, name='pay')
]