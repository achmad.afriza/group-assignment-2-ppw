from django.contrib import admin
from .models import CouponCut, History

# Register your models here.
admin.site.register(CouponCut)
admin.site.register(History)