from django.db import models

# Create your models here.
class Product(models.Model):
    id = models.CharField(max_length=6, primary_key=True)
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    stock = models.PositiveSmallIntegerField(default=0)
    description = models.TextField(max_length=100)
    img_url = models.ImageField(upload_to='img/items/', blank=True, null=True)
    slug = models.SlugField(default=name)

    def __str__(self):
        return self.name
