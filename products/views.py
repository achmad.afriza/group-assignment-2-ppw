from django.shortcuts import render
from django.conf import settings
from .models import Product
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
from django.http import HttpResponse
import json
from cart.models import Order,OrderItem
# Create your views here.
def ProductView(request):
    return render(request, 'products/itemsCatalog.html')

def Json_Response(request):
    data = serialize('json', Product.objects.all())
    return HttpResponse(json.dumps(data), content_type="application/json")