from django.urls import path, include
from . import views

app_name = 'products'

urlpatterns = [
    path('', views.ProductView),
    path('json/', views.Json_Response)

]