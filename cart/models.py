from django.db import models
from register.models import Profile
from products.models import Product
# Create your models here.
class OrderItem(models.Model):
    owner = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    product = models.OneToOneField(Product, on_delete=models.SET_NULL, null=True)
    ammount = models.PositiveIntegerField(default=1)
    date_added = models.DateTimeField(auto_now=True)
    

    def __str__(self):
        return self.product.name

class Order(models.Model):
    ref_code = models.CharField(max_length=15)
    owner = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    items = models.ManyToManyField(OrderItem)
    

    def get_cart_items(self):
        return self.items.all()

    def get_cart_total(self):
        return sum([item.product.price for item in self.items.all()])

    def __str__(self):
        return '{0} - {1}'.format(self.owner, self.ref_code)