from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
import json
from .models import Order, OrderItem
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
from django.views.decorators.csrf import csrf_exempt
from cart.models import Order,OrderItem
from register.models import Profile
from products.models import Product
import random
import string
from datetime import date
import datetime

# Create your views here.

def generate_order_id():
    date_str = date.today().strftime('%Y%m%d')[2:] + str(datetime.datetime.now().second)
    rand_str = "".join([random.choice(string.digits) for count in range(3)])
    return date_str + rand_str


@csrf_exempt
def CartView(request):
    if request.method == "POST":
        if request.POST["task"] == "del":    
            cart_item = request.POST['cart_item']
            item_to_delete = OrderItem.objects.filter(id=cart_item)
            print(item_to_delete)
            if item_to_delete.exists():
                item_to_delete[0].delete()

        else:  
            item_id = request.POST["data"]
            user_profile = get_object_or_404(Profile, user=request.user)
            product = Product.objects.filter(id=item_id).first()
            order_item, status= OrderItem.objects.get_or_create(product=product, owner=user_profile)
            user_order, status = Order.objects.get_or_create(owner=user_profile)
            user_order.items.add(order_item)
            user_order.ref_code = generate_order_id()
            user_order.save()


        # item_name = request.POST["cart_item"]
        # cart_item = CartModel.objects.get(name = item_name)
        # if request.POST['task'] == "del":
        #     print("remove item")
        #     # cart_item.delete()
        # else:
        #     if cart_item.stock > 1:
        #         print("reduce it")
        #         # cart_item.stock -= 1
        #         # cart_item.save()
        #     else:
        #         # cart_item.delete()
        #         print("remove item")
    
    return render(request, 'cart/cart.html')

def Json_Response(request):
    user_profile = Profile.objects.get(user=request.user)
    user_order = Order.objects.get(owner=user_profile)
    data = []
    for i in range (user_order.items.all().count()):
        data.append({
            'name': user_order.items.all()[i].product.name, 
            'price' : user_order.items.all()[i].product.price,
            'item_id' : user_order.items.all()[i].product.id,
            'stock': user_order.items.all()[i].product.stock,
            'Order_refcode': user_order.ref_code
            })
    # try:
    #     total_price=0
    #     total_item_bought = CartModel.objects.all().count()
    #     for i in range(total_item_bought):
    #         stock = CartModel.objects.all()[i].stock
    #         price = CartModel.objects.all()[i].price
    #         total_price += stock * price   

    #     response_data = {
    #         'total_price' : total_price
    #     }

    # except:
    #     response_data = {
    #         'total_price' : 0
    #     }

    # data = serialize('json', CartModel.objects.all())

    # json_resp = {
    #     'data' : data,
    #     'total' : response_data
    # }
    return HttpResponse(json.dumps(data), content_type="application/json")

