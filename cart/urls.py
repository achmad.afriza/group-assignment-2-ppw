from django.urls import path, include
from . import views

app_name = 'ShoppingCart'

urlpatterns = [
    path('', views.CartView),
    path('json/', views.Json_Response),
]
