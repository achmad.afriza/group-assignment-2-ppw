from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from products.models import Product

class ReviewModel(models.Model):
    name = models.OneToOneField(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=20)
    rating = models.IntegerField(default=5)
    message = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)
    item = models.ForeignKey(Product, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.title

# class Item(models.Model):
#     name = models.CharField(max_length=20)
#     code = models.CharField(max_length=20)
#     description = models.CharField(max_length=100)
#     price = models.DecimalField(max_digits=9)
#     size = models.IntegerField(default=40)