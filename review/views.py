from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string
from review.models import ReviewModel
from review.forms import ReviewForm
from django.contrib.auth.models import User

def review_list(request):
    reviews = ReviewModel.objects.all()
    return render(request, 'review.html', {'reviews':reviews})

def save_review_form(request, form, template_name):
    #userr = User.objects.get(username=request.user)
    #user_name = ReviewModel.objects.get(name=userr)
    #if userr.is_authenticated:
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            reviews = ReviewModel.objects.all()
           # data['name'] = user_name
            data['html_review_list'] = render_to_string('includes/partial_review_list.html', {
                'reviews': reviews
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

def review_create(request):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
    else:
        form = ReviewForm()
    return save_review_form(request, form, 'includes/partial_review_create.html')

def review_update(request, pk):
    review = get_object_or_404(ReviewModel, pk=pk)
    if request.method == 'POST':
        form = ReviewForm(request.POST, instance=review)
    else:
        form = ReviewForm(instance=review)
    return save_review_form(request, form, 'includes/partial_review_update.html')

def review_delete(request, pk):
    review = get_object_or_404(ReviewModel, pk=pk)
    data = dict()
    if request.method == 'POST':
        review.delete()
        data['form_is_valid'] = True  
        reviews = ReviewModel.objects.all()
        data['html_review_list'] = render_to_string('includes/partial_review_list.html', {
            'reviews': reviews
        })
    else:
        context = {'review': review}
        data['html_form'] = render_to_string('includes/partial_review_delete.html',
            context,
            request=request,
        )
    return JsonResponse(data)
