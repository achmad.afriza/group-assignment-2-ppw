from django import forms
from django.forms import ModelForm
from review.models import ReviewModel

RATE_CHOICES = [
    (1,1),
    (2,2),
    (3,3),
    (4,4),
    (5,5),
]

class ReviewForm(ModelForm):
    rating = forms.ChoiceField(
        required = True,
        widget = forms.RadioSelect,
        choices = RATE_CHOICES
    )
    class Meta:
        model = ReviewModel
        fields = [
            'name',
            'title',
            'rating',
            'message',
        ]