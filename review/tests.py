from django.test import TestCase,Client
from django.urls import resolve
from review.views import review_list, review_create, review_update, review_delete
from review.models import ReviewModel
from django.contrib.auth.models import User
from selenium import webdriver
import time
import unittest

# class ReviewTest(TestCase):

#     def test_review_url_is_exist(self):
#         response = Client().get('/review/')
#         self.assertEqual(response.status_code, 200)

#     def test_review_using_review_list_func(self):
#         found = resolve('/review/')
#         self.assertEqual(found.func, review_list)

#     def test_review_using_review_create_func(self):
#         found = resolve('/review/create')
#         self.assertEqual(found.func, review_create)

#     def test_review_using_review_update_func(self):
#         found = resolve('/review/1/update')
#         self.assertEqual(found.func, review_update)

#     def test_review_using_review_delete_func(self):
#         found = resolve('/review/1/delete')
#         self.assertEqual(found.func, review_delete)

#     def test_review_using_correct_template(self):
#         response = Client().get('/review/')
#         self.assertTemplateUsed(response, 'review.html')

#     def test_model_can_create_new_review(self):
#         admin = User.objects.create(username="admin")
#         review = ReviewModel.objects.create(name=admin, title='title', rating=1, message="hai")
#         counting_all_available_review = ReviewModel.objects.all().count()
#         self.assertEqual(counting_all_available_review, 1)

    
# class FunctionalTest(unittest.TestCase):
#     def setUp(self):
#         self.driver  = webdriver.Chrome("C:/Users/Lenovo/Downloads/chromedriver_win32")

#     def tearDown(self):
#         self.driver.quit()

#     def test_can_add_review(self):
#         self.driver.get('http://localhost:8000/review/')
#         time.sleep(5)
#         button1 = self.driver.find_element_by_id('addbutton')
#         button2 = self.driver.find_element_by_id('createbutton')
#         name = self.driver.find_element_by_id('id_name')
#         title = self.driver.find_element_by_id('id_title')
#         rating = self.driver.find_element_by_id('id_rating')
#         message = self.driver.find_element_by_id('id_message')

#         time.sleep(5)
#         button1.submit()
#         name.send_keys('name')
#         title.send_keys('title')
#         rating.send_keys('rating')
#         message.send_keys('message')
#         time.sleep(5)
#         button2.submit()

#         time.sleep(5)
#         self.assertIn('name', self.driver.page_source)
#         self.assertIn('title', self.driver.page_source)
#         self.assertIn('rating', self.driver.page_source)
#         self.assertIn('message', self.driver.page_source)

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')