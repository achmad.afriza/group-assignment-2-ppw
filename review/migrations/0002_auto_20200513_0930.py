# Generated by Django 2.2.5 on 2020-05-13 02:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviewmodel',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
