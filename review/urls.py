from django.urls import path
from review.views import review_list, review_create, review_update, review_delete


urlpatterns = [
    path('', review_list, name='review_list'),
    path('create', review_create, name='review_create'),
    path('<int:pk>/update', review_update, name='review_update'),
    path('<int:pk>/delete', review_delete, name='review_delete'),
]
